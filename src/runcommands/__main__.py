import sys

from .run import run


main = run.console_script


if __name__ == "__main__":
    sys.exit(main())
